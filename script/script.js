// "use strict";

/////////////////////////////////////////////////////////////////

// Funkcija za Login (Email, Password)

function login(email, password) {
    if (email == 'test@test.com' && password == 'tajnasifra') {
        window.open('home.html', '_self')
        // window.open('https://codepen.io/miljana-milentijevic/full/LYeegvG')
    }
    else {
        alert('Neispravni kredencijali!')
        // alert('Invalid credentials!')
    }
}

/////////////////////////////////////////////////////////////////

// Password - Toggling eye-icon ON/OFF

const visibilityToggle = document.querySelector('.visibility');

const input = document.querySelector('.password');

var password = true;

visibilityToggle.addEventListener('click', function() {
  if (password) {
    input.setAttribute('type', 'text');
    visibilityToggle.innerHTML = "visibility_off";
  } else {
    input.setAttribute('type', 'password');
    visibilityToggle.innerHTML = "visibility";
  }
  password = !password;
})
